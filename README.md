# ポータルサイトっぽいもの

現状ログインフォームは何を入力してもログインできます．
![スクリーンショット_2020-01-15_16.21.23](/uploads/f36c273ab88a68d6357072d6deec7c50/スクリーンショット_2020-01-15_16.21.23.png)

Vuetifyで簡単に作れました．
![スクリーンショット_2020-01-15_16.21.36](/uploads/234a4e60bc7405730ac931ae5a451d71/スクリーンショット_2020-01-15_16.21.36.png)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
